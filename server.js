// Se importa la librería
const express = require('express');
// Se arranca el framework express
const app = express();
//Se importan los modulos de la app
const jwt = require('jsonwebtoken');
const authController = require("./controllers/AuthController");
const accountController = require("./controllers/AccountController");
const movementController = require("./controllers/MovementController");

// Se importan las propiedades
const jwtSecret = process.env.JWT_SECRET;

// Se habilita el cors
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type, x-access-token");

 next();
}
app.use(enableCORS);

// Se configura express para que procese siempre el body como json
app.use(express.json());

// Se levanta la app en el puerto definido
var server = app.listen(3000, function () {
  // Se define un puerto
  const port = process.env.PORT || 3000;
  console.log('Listening at port %s', port);
});

// Se crea una funcion middleware para comprobar la autenticación en las url que corresponda
app.use(function (req, res, next) {
  if ('/my-bank/v1/login' === req.url || '/my-bank/_health' === req.url
    || '/my-bank/v1/register' === req.url || '/favicon.ico'  === req.url
    || req.method === 'OPTIONS') {
    next()
  } else {
    var token = req.headers['x-access-token'];
    if (!token) {
      return res.status(403).send({ auth: false, mensaje: 'Usuario no autenticado.' });
    } else {
      jwt.verify(token, jwtSecret, function(err, decoded) {
        if (err) {
          return res.status(403).send({ auth: false, mensaje: 'Usuario no autenticado.' });
        } else {
          // Si la autenticación es correcta, se añade el id del usuario a la request
          req.userId = decoded.id;
          next()
        }
      });
    }
  }
})

// Url que comprueba que el api responde correctamente
app.get('/my-bank/_health', function (req, res) {
  res.status(200).send({"msg" : "API Works."});
});

//Enrutado AuthController
app.post('/my-bank/v1/login', authController.loginV1);
app.post('/my-bank/v1/register', authController.registerV1);

// Enrutado AccountController
app.get('/my-bank/v1/accounts', accountController.getAccountsByUserIdV1);
app.post('/my-bank/v1/accounts', accountController.createAccountV1);
app.delete('/my-bank/v1/accounts/:id', accountController.deleteAccountV1);

// Enrutado MovementsController
app.get('/my-bank/v1/accounts/:id/movements', movementController.getMovementsByAccountIdV1);
app.post('/my-bank/v1/accounts/:id/movements', movementController.doMovementV1);

module.exports.server = server;
