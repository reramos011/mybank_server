// Se importa la librería bcrypt
const bcrypt = require('bcrypt');

function hash(data) {
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFormUserInPlainText, passwordFromDBHash) {
  return bcrypt.compareSync(passwordFormUserInPlainText, passwordFromDBHash);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
