# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /myBank

# Copia de archivos de carpeta local a apitechu
ADD . /myBank

# Se instalan las dependencias del proyecto (se esta ejecutando la info del package.json del codigo que ha sido copiado a /myBank)
RUN npm install

# Puerto en el que se expone
EXPOSE 3000

# Comando de inicialización
CMD ["npm", "start"]
