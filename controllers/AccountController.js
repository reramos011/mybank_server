//Se importan los modulos necesarios
const accountService = require("../services/Account");
const movementService = require("../services/Movement");
const shortid = require('shortid');
const bankHelpers = require('iban-constructor/lib/bankHelpers');

function getAccountsByUserIdV1(req, res) {
  accountService.findByUserId(req.userId, function (err, accounts) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (!accounts) return res.status(204).send({"mensaje" : "No hay cuentas para ese usuario"});

    res.status(200);
    res.send(accounts);
  });
}

function createAccountV1(req, res) {
  // Validar que vienen los campos necesarios
  if (!req.userId) {
     return res.status(422).send({"mensaje" : "Campos obligatorios no informados"});
  }

  var newAccount = {
    "id": shortid.generate(),
    "iban": bankHelpers.generateIbanForBank('41189'),
    "user_id": req.userId,
    "balance": 0.00
  };

  accountService.create(newAccount, function (err, account) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    console.log('creada cuenta');
    console.log(account);
    // Se crea un movimiento inicial en la cuenta
    var initialMovement = {
      account_id : account.id,
      date: new Date(),
      label: 'INGRESO',
      description: 'Ingreso de apertura',
      amount: 100,
      balance: 100,
      category: '9999',
      category_desc: 'Pendiente de categorizar'
    };
    movementService.create(initialMovement, function (err, movement) {
      if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
      console.log('creado movmiento');
      console.log(initialMovement);
      account.balance = movement.balance;
      accountService.update(account, function (err, finalAccount) {
        if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
        accountService.findByUserId(req.userId, function (err, accounts) {
          if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
          if (!accounts) return res.status(204).send({"mensaje" : "No hay cuentas para ese usuario"});
          res.status(200);
          res.send(accounts);
        });
      });
    });
  });
}

// Elimina una cuenta y sus movimientos
function deleteAccountV1(req, res) {
  // Se recupera la cuenta asociada al id
  accountService.findOne(req.params.id, function (err, account) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (account) {
      // Se valida que el usuario logado es propietario de la cuenta
      if (req.userId != account.user_id) {
        return res.status(422).send({"mensaje" : "La operacion indicada no es valida."});
      } else {
        // Se elimina la cuenta
        accountService.remove(account, function (err, oldAccount) {
          if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});

          // se eliminan los movimientos asociados a la cuenta
          movementService.removeByAccountId(oldAccount.id, function (err, resp) {
            if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
            res.status(200);
            res.send(oldAccount);
          });
        });
      }
    } else {
      return res.status(422).send({"mensaje" : "La cuenta indicada no es valida."});
    }
  });
}

module.exports.getAccountsByUserIdV1 = getAccountsByUserIdV1;
module.exports.createAccountV1 = createAccountV1;
module.exports.deleteAccountV1 = deleteAccountV1;
