////Se importan los modulos necesarios
const movementService = require("../services/Movement");
const accountService = require("../services/Account");


function getMovementsByAccountIdV1(req, res) {
  // Se recupera la cuenta asociada al id para validar que el usuario es el propietario
  accountService.findOne(req.params.id, function (err, account) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (account) {
      // Se valida que el usuario logado es propietario de la cuenta
      if (req.userId != account.user_id) {
        return res.status(422).send({"mensaje" : "La operacion indicada no es valida."});
      } else {
        movementService.findByAccountId(req.params.id, function (err, movements) {
          if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
          if (!movements) return res.status(204).send({"mensaje" : "No hay movimientos para esta cuenta"});

          res.status(200);
          res.send(movements);
        });
      }
    } else {
      return res.status(422).send({"mensaje" : "La cuenta indicada no es valida."});
    }
  });
}

function doMovementV1(req, res) {
  // Validar que vienen los campos necesarios
  if (!req.body.type || !req.body.description || !req.body.amount || !req.params.id) {
     return res.status(422).send({"mensaje" : "Campos obligatorios no informados"});
  }

  // Se recupera la cuenta asociada al id
  accountService.findOne(req.params.id, function (err, account) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (account) {
      // Se valida que el usuario logado es propietario de la cuenta
      if (req.userId != account.user_id) {
        return res.status(422).send({"mensaje" : "La operacion indicada no es valida."});
      } else {
        // Se registra el movimiento y se actualiza el balance de la cuenta
        var type;
        var amount;
        var newBalance;
        if (req.body.type === 'RETIRADA') {
          type = 'Reintegro';
          newBalance = Number(account.balance) - Number(req.body.amount);
          amount = '-' + req.body.amount;
        } else if (req.body.type === 'INGRESO') {
          type = 'Ingreso';
          newBalance = Number(account.balance) + Number(req.body.amount);
          amount = '' + req.body.amount;
        } else {
          return res.status(422).send({"mensaje" : "La operación indicada no es válida."});
        }
        var newMovement = {
          account_id : req.params.id,
          date: new Date(),
          label: type,
          description: req.body.description,
          amount: amount,
          balance: newBalance,
          category: '9999',
          category_desc: 'Pendiente de categorizar'
        };
        movementService.create(newMovement, function (err, movement) {
          account.balance = movement.balance;
          accountService.update(account, function (err, account) {
            if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});

            // Si todo ha ido correctamente, devolvemos un 201
            return res.status(201).send({"mensaje" : "La operacion se ha realizado correctamente"});
          });
        });
      }
    } else {
      return res.status(422).send({"mensaje" : "La cuenta indicada no es valida."});
    }
  });
}

module.exports.getMovementsByAccountIdV1 = getMovementsByAccountIdV1;
module.exports.doMovementV1 = doMovementV1;
