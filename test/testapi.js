// Librería base de test unitarios
const mocha = require('mocha');
// Librería para realizar aserciones
const chai = require('chai');
// Librería para usar un cliente http en las aserciones de chai
const chaihttp = require('chai-http');
const server = require('../server');
const testUser = process.env.TEST_USER;
const testPass = process.env.TEST_PASS;

chai.use(chaihttp);

var should = chai.should();
var token;

describe('Tests de inicializacion y arranque', function () {
  it('API arrancada y responde correctamente', function (done) {
    chai.request('http://localhost:3000')
      .get('/my-bank/_health')
      .end(
        function(err, res) {
          // Se realiza una aserción para verificar que la petición devuelve status 200
          res.should.have.status(200);
          // Se realiza una aserción para verificar que devuelve una propiedad msg con el valor esperado
          res.body.msg.should.be.eql("API Works.");
          done();
        }
      );
    });
});

describe('Tests de autenticacion y acceso', function () {
  it('Login correcto', function (done) {
    chai.request('http://localhost:3000')
      .post('/my-bank/v1/login')
      .send({"email": testUser, "password": testPass})
      .end(
        function(err, res) {
          // Se realiza una aserción para verificar que la petición devuelve status 200
          res.should.have.status(200);
          // Se realiza una aserción para verificar que devuelve el usuario esperado y un token
          res.body.email.should.be.eql(testUser);
          res.body.should.have.property('token');
          token = res.body.token;
          done();
        }
      );
    });

  it('Login incorrecto', function (done) {
    chai.request('http://localhost:3000')
      .post('/my-bank/v1/login')
      .send({"email": testUser, "password": "aaaa"})
      .end(
        function(err, res) {
          // Se realiza una aserción para verificar que la petición devuelve status 401
          res.should.have.status(401);
          // Se realiza una aserción para verificar que devuelve una propiedad msg con el valor esperado
          res.body.mensaje.should.be.eql("Login incorrecto");
          done();
        }
      );
    });
});

describe('Tests cuentas', function () {
  it('Consulta de cuentas', function (done) {
    chai.request('http://localhost:3000')
      .get('/my-bank/v1/accounts')
      .set('x-access-token', token)
      .end(
        function(err, res) {
          // Se realiza una aserción para verificar que la petición devuelve status 200
          res.should.have.status(200);
          // Se realiza una aserción para verificar que devuelve un array
          res.body.should.be.a('array');
          done();
        }
      );
    });
});
