//Se importan los modulos necesarios
const crypt = require('../crypt');
const requestJson = require('request-json');
const jwt = require('jsonwebtoken');
const userService = require("../services/User");
const accountService = require("../services/Account");
const movementService = require("../services/Movement");
const shortid = require('shortid');
const bankHelpers = require('iban-constructor/lib/bankHelpers');

// Se importan las propiedades
const jwtSecret = process.env.JWT_SECRET;

function loginV1(req, res) {
  userService.findOne(req.body.email, function (err, user) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (!user) return res.status(401).send({"mensaje" : "Login incorrecto"});

    // Se comprueba el password
    if(crypt.checkPassword(req.body.password, user.password)) {
      // Si el password es correcto, se asigna un token de usuario
      var token = jwt.sign({ id: user.id }, jwtSecret, {
        expiresIn: 86400 // expires in 24 hours
      });
      user.token = token;
      delete user.password;
      return res.status(200).send(user);
    } else {
      return res.status(401).send({"mensaje" : "Login incorrecto"});
    }
  });
}

function registerV1(req, res) {
  // Validar que vienen los campos necesarios
  if (!req.body.first_name || !req.body.last_name || !req.body.email || !req.body.password) {
     return res.status(422).send({"mensaje" : "Campos obligatorios no informados"});
  }

  // Validar que no existe un usuario para ese email
  userService.findOne(req.body.email, function (err, user) {
    if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
    if (!user) {
      var hashedPassword = crypt.hash(req.body.password);
      userService.create({
        id: shortid.generate(),
        first_name : req.body.first_name,
        last_name: req.body.last_name,
        email : req.body.email,
        password : hashedPassword
      },
      function (err, user) {
        if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
        console.log('creado usuario');
        console.log(user);
        // Si el registro se realiza correctamente, generamos un token para el usuario registrado y damos de alta una cuenta
        var newAccount = {
          "id": shortid.generate(),
          "iban": bankHelpers.generateIbanForBank('41189'),
          "user_id": user.id,
          "balance": 0.00
        };
        accountService.create(newAccount, function (err, account) {
          if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
          console.log('creada cuenta');
          console.log(account);
          // Se crea un movimiento inicial en la cuenta
          var initialMovement = {
            account_id : account.id,
            date: new Date(),
            label: 'INGRESO',
            description: 'Ingreso de apertura',
            amount: 100,
            balance: 100,
            category: '9999',
            category_desc: 'Pendiente de categorizar'
          };
          movementService.create(initialMovement, function (err, movement) {
            if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
            console.log('creado movmiento');
            console.log(initialMovement);
            account.balance = movement.balance;
            accountService.update(account, function (err, finalAccount) {
              if (err) return res.status(500).send({"mensaje" : "Servicio no disponible en este momento. Intentelo de nuevo pasados unos minutos"});
              console.log('actualizada cuenta');
              console.log(finalAccount);
              var token = jwt.sign({ id: user.id }, jwtSecret, {
                expiresIn: 86400 // expires in 24 hours
              });
              return res.status(201).send({ token: token });
            });
          });
        });
      });
    } else {
      return res.status(422).send({"mensaje" : "El email utilizado ya se encuentra registrado."});
    }
  });
}

module.exports.loginV1 = loginV1;
module.exports.registerV1 = registerV1;
