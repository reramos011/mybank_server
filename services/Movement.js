// Se carga el fichero de configuracion
require('dotenv').config();
// Se importa el cliente http
const requestJson = require('request-json');
// Url base Mlab
const baseMlabURL = process.env.MLAB_URL;
// MLab apiKey
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Recupera movimientos filtrando por el iban de una cuenta
function findByAccountId(accountId, callbackFunction) {

  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"account_id" : "' + accountId + '"}';
  var sort = 's={"date": -1}';
  var url = "movement?" + query + "&" + sort + "&" + mLabAPIKey;
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        if (body.length > 0) {
          var movements = body;
          return callbackFunction(err, movements);
        } else {
          return callbackFunction(null, null);
        }
      }
    }
  );
}

// Crea un nuevo objeto movement en mongo
function create(movement, callbackFunction) {
  var httpClient = requestJson.createClient(baseMlabURL);
  var url = "movement?" + mLabAPIKey;
  httpClient.post(url, movement,
      function(err, resMLab, body) {
        if (err) {
          console.log(err);
          return callbackFunction(err);
        } else {
          return callbackFunction(err, movement);
        }
      }
    );
}

// Crea un nuevo objeto movement en mongo
function removeByAccountId(accountId, callbackFunction) {
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = '&q={"account_id" : "' + accountId + '"}';
  var url = "movement?" + mLabAPIKey + query;
  console.log(url);
  httpClient.put(url, [],
      function(err, resMLab, body) {
        if (err) {
          console.log(err);
          return callbackFunction(err);
        } else {
          return callbackFunction(err, accountId);
        }
      }
    );
}


module.exports.findByAccountId = findByAccountId;
module.exports.create = create;
module.exports.removeByAccountId = removeByAccountId;
