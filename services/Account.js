// Se carga el fichero de configuracion
require('dotenv').config();
// Se importa el cliente http
const requestJson = require('request-json');
// Url base Mlab
const baseMlabURL = process.env.MLAB_URL;
// MLab apiKey
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Recupera las cuentas filtrando por el userId
function findByUserId(userId, callbackFunction) {

  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"user_id" : "' + userId + '"}';
  var url = "account?" + query + "&" + mLabAPIKey;

  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        if (body.length > 0) {
          var response = body;
          return callbackFunction(err, response);
        } else {
          return callbackFunction(null, null);
        }
      }
    }
  );
}

// Recupera una cuenta filtrando por su identificador unico
function findOne(id, callbackFunction) {

  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"id" : "' + id + '"}';
  var url = "account?" + query + "&" + mLabAPIKey;
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        if (body.length > 0) {
          var response = body[0];
          return callbackFunction(err, response);
        } else {
          return callbackFunction(null, null);
        }
      }
    }
  );
}

// Crea un nuevo objeto account en mongo
function create(newAccount, callbackFunction) {
  var httpClient = requestJson.createClient(baseMlabURL);
  var url = "account?" + mLabAPIKey;
  httpClient.post(url, newAccount,
    function(err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        return callbackFunction(err, newAccount);
      }
    }
  );
}

// Actualiza la cuenta
function update(account, callbackFunction) {
  delete account._id;
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = '&q={"id" : "' + account.id + '"}&u=false';
  var url = "account?" + mLabAPIKey + query;
  console.log(url);
  httpClient.put(url, account,
    function(err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        console.log(body);
        return callbackFunction(err, account);
      }
    }
  );
}

// Elimina la cuenta
function remove(account, callbackFunction) {
  delete account._id;
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = '&q={"id" : "' + account.id + '"}';
  var url = "account?" + mLabAPIKey + query;
  console.log(url);
  httpClient.put(url, [],
    function(err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        console.log(body);
        return callbackFunction(err, account);
      }
    }
  );
}

module.exports.findByUserId = findByUserId;
module.exports.findOne = findOne;
module.exports.update = update;
module.exports.create = create;
module.exports.remove = remove;
