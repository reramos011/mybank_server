// Se carga el fichero de configuracion
require('dotenv').config();
// Se importa el cliente http
const requestJson = require('request-json');
// Url base Mlab
const baseMlabURL = process.env.MLAB_URL;
// MLab apiKey
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Recupera el usuario filtrando por el email
function findOne(email, callbackFunction) {

  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"email" : "' + email + '"}';
  var url = "user?" + query + "&" + mLabAPIKey;
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        console.log("Error en el acceso a base de datos", err);
        return callbackFunction(err);
      } else {
        if (body.length > 0) {
          var user = body[0];
          delete user._id;
          return callbackFunction(err, user);
        } else {
          return callbackFunction(null, null);
        }
      }
    }
  );
}

// Crea un nuevo objeto user en mongo
function create(newUser, callbackFunction) {
  var httpClient = requestJson.createClient(baseMlabURL);
  var url = "user?" + mLabAPIKey;
  httpClient.post(url, newUser,
      function(err, resMLab, body) {
        if (err) {
          console.log(err);
          return callbackFunction(err);
        } else {
          return callbackFunction(err, newUser);
        }
      }
    );
}

module.exports.findOne = findOne;
module.exports.create = create;
